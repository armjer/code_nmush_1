<?php

namespace controllers;
use core\Controller;
use core\View;

/**
 * Class SiteController
 */
class SiteController extends Controller
{
    public function indexAction()
    {
        $query = $this->database->prepare('select * from visitors');
        $query->execute([]);
        $data = $query->fetchAll();

        return View::render('site/index', [
            'a' => 5,
            'data' => $data
        ]);
    }

    public function testAction()
    {
        echo 'test page';
        print_r($_REQUEST);
    }
}