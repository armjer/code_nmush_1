<?php

use core\router\Route;
use core\router\RouteParam;

Route::get('/site/index', function(RouteParam $site, RouteParam $date) {

})->to('site::index');

Route::get('/test', function(RouteParam $site, RouteParam $date) {

})->to('site::test');
